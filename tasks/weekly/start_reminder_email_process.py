import requests
from requests.auth import HTTPBasicAuth
import json
import os

url = os.getenv("APIGATEWAY_INTERNAL_BASEURL") + "/login"
auth = HTTPBasicAuth(os.getenv("SYSTEM_USER"), os.getenv("SYSTEM_PASSWORD"))
payload={}


response = requests.request("GET", url, auth=auth, data=payload)

apikey = response.json().get("apikey")
f = open("/tmp/cron.log", "w")
f.write(f"Got apikey: {apikey}\n")
f.close()



url = os.getenv("APIGATEWAY_INTERNAL_BASEURL") + "/es/cmd/sendNextWeekReminderEmails"

payload = json.dumps({})
headers = {
  'apikey': apikey,
  'Content-Type': 'application/json'
}

response = requests.request("POST", url, headers=headers, data=payload)

print(response.text)
f = open("/tmp/cron.log", "a")
f.write(f"Got response:\n {response.text}\n")
f.close()
