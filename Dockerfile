FROM registry.gitlab.com/crowdsoft-foundation/various/python-base-image:production

COPY tasks /etc/periodic/
RUN chmod 777 /etc/periodic/ -R && chmod +x /etc/periodic/ -R
COPY startup.sh /usr/local/startup.sh
COPY crontab /etc/crontabs/root

RUN chmod 777 /usr/local/startup.sh && chmod +x /usr/local/startup.sh
CMD /usr/local/startup.sh